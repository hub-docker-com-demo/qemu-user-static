import argparse, pexpect, sys

parser = argparse.ArgumentParser(description='Quemu for OpenWRT.')
parser.add_argument('command_line', metavar='C', nargs='?',
                    help='command line for qemu',
                    default='qemu-system-arm -M realview-eb-mpcore -kernel openwrt-realview-vmlinux-initramfs.elf -nographic -nic tap')
args = parser.parse_args()

child = pexpect.spawn(args.command_line)
# child.logfile = sys.stdout
child.logfile_read = sys.stdout
child.expect('Please press Enter to activate this console.')
child.sendline('')
# child.expect('root@(none):/#')
child.expect('root.*:')
child.sendline('uname -a')
child.expect('root.*:')
child.sendline('cat /proc/cmdline')
child.expect('root.*:')
child.sendline('ifconfig') # Internet connection not working!
child.expect('root.*:')
child.sendline('route -n') # Internet connection not working!
child.expect('root.*:')
child.sendline('ping -c 1 github.com') # Internet connection not working!
child.expect('root.*:')
# child.sendline('opkg update') # Internet connection not working!
# child.expect('root.*:')
# child.sendline('opkg')
# child.expect('root.*:')
# child.sendline('halt')
# child.expect(pexpect.EOF)
# print child.before;
if child.isalive():
    child.sendline('halt') # Try to ask cadaver child to exit.
    # child.expect('reboot: System halted', timeout=120)
    child.expect('reboot: .*') # 'System halted' or 'Power down'
    # print(child.before)
    child.close()
# Print the final state of the child. Normally isalive() should be FALSE.
print
if child.isalive():
    print('Child did not exit gracefully.')
else:
    print('Child exited gracefully.')
print(child.exitstatus, child.signalstatus)